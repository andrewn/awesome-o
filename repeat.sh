#!/usr/bin/env bash

while true; do
  date > README.md
  git add README.md
  git commit -m "$(date)"
  time git push origin
done
